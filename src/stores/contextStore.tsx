import { createContext, useContext, useReducer } from "react";

export const initialState = {
  name: "Rodrigo",
  age: "26",
  city: "Adrogue"
};

export const storeState = createContext(null);
export const storeDispatch = createContext(null);

export const useStore = () => {
  return useContext(storeState);
};
export const useStoreDispatch = () => {
  return useContext(storeDispatch);
};

export const whiteReducer = (state, action) => {
  return {
    ...state,
    name: action.payload
  };
};

export const inicialReducer = (state, action) => {

  console.log("CONTEXT ACTION", action);

  if (action.type === "white") {
    return whiteReducer(state, action);
  }

  if (action.type === "name") {
    return {
      ...state,
      name: action.payload
    };
  }

};

export const StoreProvider = ({ children }) => {

  const [state, dispatch] = useReducer(inicialReducer, initialState);

  return (
    <storeState.Provider value={state}>
      <storeDispatch.Provider value={dispatch}>
        {children}
      </storeDispatch.Provider>
    </storeState.Provider>
  );

};
