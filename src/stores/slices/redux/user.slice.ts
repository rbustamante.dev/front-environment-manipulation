import { createSlice } from "@reduxjs/toolkit";
import { userGetDataFulfilled, userGetDataPending, userGetDataRejected } from "../../reducers/user/user.reducer";
import { userGetData } from "../../../services/middlewares/user.middleware";

const initialState = {
  data: {},
  status: "idle",
  error: false,
  message: "prueba"
};

const userSlice = createSlice({
  name: "user",
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(userGetData.pending, (state, action) => {
      userGetDataPending(state, action);
    });
    builder.addCase(userGetData.fulfilled, (state, action) => {
      userGetDataFulfilled(state, action);
    });
    builder.addCase(userGetData.rejected, (state, action) => {
      userGetDataRejected(state, action);
    });
  }
});

export default userSlice.reducer;