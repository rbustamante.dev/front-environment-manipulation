export const userGetDataPending = (state, action) => {
    state.status = "loading";
    state.data = null;
    console.log("Pending: ", action)
}

export const userGetDataRejected = (state, action) => {
    state.status = "error";
    state.error = true;
    console.log("Rejected: ", action)
}

export const userGetDataFulfilled = (state, action) => {
    state.status = "success";
    state.data = action.payload;
    console.log("Fulfilled: ", action)
}