import { configureStore } from "@reduxjs/toolkit";
import { NotificationMiddleware } from "../services/middlewares/notification.middleware";
import userSlice from "./slices/redux/user.slice";

export const store = configureStore({
  reducer: { main: userSlice },
  middleware: (getDefaultMiddleware) => { return getDefaultMiddleware().concat(NotificationMiddleware); }
});

export type AppDispatch = typeof store.dispatch;