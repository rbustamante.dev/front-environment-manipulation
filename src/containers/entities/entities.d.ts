export type TypeWithKey<T> = {
    [key: string]: T
};

export type Prueba = {
    name: string,
    city: string[]
}

export interface IPrueba { // incorrecto, utilizar type
    name: string,
    city: Array<string> // incorrecto, utilizar string[]
}
