import { createBrowserRouter } from "react-router-dom";
import { ZustandScreen } from "../screens/zustand.screen";
import { MainScreen } from "../screens/screens.module";

export const router = createBrowserRouter([
  { path: "/", element: <MainScreen></MainScreen> },
  { path: "/zustand", element: <ZustandScreen></ZustandScreen> }
]);
