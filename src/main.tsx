import ReactDOM from "react-dom/client";
import { RouterProvider } from "react-router-dom";
import { router } from "./routes/router";
import { MainInterceptor } from "./services/interceptors/main.interceptor";
import { StoreProvider } from "./stores/contextStore";
import { Provider } from "react-redux";
import { store } from "./stores/reduxStore";
import axios from "axios";
import "./main.css";

MainInterceptor(axios);
ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <StoreProvider>
    <Provider store={store}>
      <RouterProvider router={router}></RouterProvider>
    </Provider>
  </StoreProvider>
);
