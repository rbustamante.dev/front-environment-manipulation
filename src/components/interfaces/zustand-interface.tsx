import { useState } from "react";
import leafnoise_logo from "../assets/leafnoise.svg";
import { NotificationProvide } from "../../services/middlewares/notification.middleware";
import { useShallow, useStore } from "../../stores/zustandStore";

export const ZustandInterface = (): JSX.Element => {

  // ZUSTAND
  const user = useStore((state) => { return state.user; }, useShallow);
  const product = useStore((state) => { return state.product; }, useShallow);

  console.log("USER", user);
  console.log("PRODUCT", product);

  const [count, setCount] = useState(0);

  const fetchUser = () => {
    user.getUsers();
  };

  const tema_actual: string = import.meta.env.ENV_STYLE as string; // correcto
  const env_actual = import.meta.env.ENV_MODE; // incorrecto, unsafe any

  return (
    <>
      <NotificationProvide></NotificationProvide>
      <div className="flex flex-col items-center justify-center bg-secondary w-full h-screen">
        <div>
          <img src={leafnoise_logo} className="w-80 mb-12" alt="logo" />
        </div>
        <h1 className='text-slate-100 mb-12 text-2xl'>Vite + React + Tailwind = Moorea Workbench</h1>
        <div className="flex flex-col items-center justify-center">
          <button onClick={() => { return setCount((count) => { return count + 1; }); }} className="text-slate-100 p-2 border-2 border-slate-200 rounded mb-6 bg-primary">
            Cuenta {count}
          </button>
          <p className='text-slate-100 text-lg font-medium'>Entorno: {env_actual}</p>
          <p className='mb-6 text-slate-100 text-lg font-medium'>Estilo: {tema_actual}</p>
          <button onClick={() => { fetchUser(); }} className="text-slate-100 p-2 border-2 border-slate-200 rounded mb-6 bg-primary">
            Pegale a la API
          </button>
          <p className='text-slate-100 text-lg font-medium'>Zustand Store : {user.status === "success" ? user.data.name : user.status}</p>
        </div>
      </div>
    </>
  );
};
