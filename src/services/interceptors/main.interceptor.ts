import { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from "axios";
import { TypeWithKey } from "containers/entities/entities";

const onRequest = (request: AxiosRequestConfig): AxiosRequestConfig => {
  console.info("Request:", request);
  return request;
};

const onResponse = (response: AxiosResponse): AxiosResponse => {
  console.info("Response:", response);
  return response;
};

const onError = (error: AxiosError): Promise<AxiosError> => {
  console.log("Error:", onErrorValidation(error.code));
  return Promise.reject(error);
};

const onErrorValidation = (error: string) => {
  const code: TypeWithKey<string> = {
    ERR_BAD_REQUEST: "Este es un error personalizado :)" // ejemplo de mensaje personalizado
  };
  return code[error];
};

export function MainInterceptor(axiosInstance: AxiosInstance): AxiosInstance {
  axiosInstance.interceptors.request.use(onRequest, onError);
  axiosInstance.interceptors.response.use(onResponse, onError);
  return axiosInstance;
}