import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export const NotificationProvide = (): JSX.Element => {
  return <ToastContainer style={{ width: "350px", height: "min-content" }} />;
};

export const NotificationMiddleware = () => {
  return next => {
    return action => {
      switch (action.type) {
        case "user/getData/fulfilled":
          toast.success("Le pegaste fuerte..", { position: toast.POSITION.TOP_RIGHT });
          break;
        case "user/getData/rejected":
          toast.error("Que miras bobo, anda pa alla..", { position: toast.POSITION.TOP_RIGHT });
          break;
      }
      return next(action);
    };
  };
};
