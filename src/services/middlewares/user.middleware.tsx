
import { createAsyncThunk } from "@reduxjs/toolkit";
import { getDataAPI } from "../endpoints/prueba.endpoint";

export const userGetData = createAsyncThunk('user/getData', async () => {
    const response = await getDataAPI().then((response) => { return response.data });
    return response;
})