module.exports = {
    root: true,
    env: {
        browser: true,
        es2021: true
    },
    extends: [
        "eslint:recommended",
        "plugin:react/recommended",
        "plugin:@typescript-eslint/recommended"
    ],
    parser: "@typescript-eslint/parser",
    parserOptions: {
        ecmaVersion: "latest",
        ecmaFeatures: {
            jsx: true,
        },
        sourceType: "module",
        tsconfigRootDir: __dirname,
        project: ['../../tsconfig.json'],
    },
    plugins: [
        "react",
        "@typescript-eslint"
    ],
    rules: {
        indent: [
            "warn",
            2
        ],
        quotes: [
            "warn",
            "double"
        ],
        semi: [
            "warn",
            "always"
        ],
        eqeqeq: [
            "warn",
            "always"
        ],
        'react/prop-types': "off",
        "arrow-body-style": ["warn", "always"],
        "@typescript-eslint/consistent-type-definitions": [
            "warn",
            "type"
        ],
        "@typescript-eslint/no-empty-interface": "warn",
        "@typescript-eslint/no-explicit-any": "warn",
        "@typescript-eslint/no-extra-non-null-assertion": "warn",
        "@typescript-eslint/no-require-imports": "warn",
        "@typescript-eslint/no-unsafe-assignment": "warn",
        "@typescript-eslint/no-useless-empty-export": "warn",
        "@typescript-eslint/no-inferrable-types": "warn",
        "@typescript-eslint/array-type": "warn",
        "@typescript-eslint/explicit-function-return-type": "warn",
        "@typescript-eslint/prefer-includes": "warn",
        "@typescript-eslint/prefer-optional-chain": "warn",
        "@typescript-eslint/prefer-enum-initializers": "warn",
        "@typescript-eslint/switch-exhaustiveness-check": "warn",
        "react/react-in-jsx-scope": "off"
    },
    settings: {
        react: {
            version: "detect"
        },
    }
}