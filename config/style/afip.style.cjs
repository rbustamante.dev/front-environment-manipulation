/** @type {import('tailwindcss').Config} */

module.exports = {
    presets: [
        require('./main.style.cjs')
    ],
    theme: {
        extend: {
            colors: {
                'primary': '#7e5bef',
                'secondary': '#B089CC',
            }
        },
    },
}
