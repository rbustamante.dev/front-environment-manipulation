# STEP 1 - Build node image
FROM node:18.14.0-alpine as builder

# STEP 1 - Set directory
WORKDIR /app

# STEP 1 - Copy project files
COPY ./package.json /app/

# STEP 1 - Install dependencies
RUN cd /app && npm set progress=false && npm install

# STEP 1 - Copy project files after install dependencies
COPY . /app

# STEP 1 - Build static files
RUN cd /app && npm run start:build

# STEP 2 - Build nginx image
FROM nginx:1.23-alpine

# STEP 2 - Remove default nginx website from image
RUN rm -rf /usr/share/nginx/html/*

# STEP 2 - From 'builder' copy website to default nginx public folder
COPY --from=builder /app/build /usr/share/nginx/html

# STEP 2 - Expose port
EXPOSE 80

# STEP 2 - Run 
CMD ["nginx", "-g", "daemon off;"]
